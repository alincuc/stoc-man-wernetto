package com.wernetto.stocman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WernettoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WernettoApplication.class, args);
    }

}
