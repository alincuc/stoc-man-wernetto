package com.wernetto.stocman.Services;

import com.wernetto.stocman.Models.ConfirmationToken;
import com.wernetto.stocman.Models.User;
import com.wernetto.stocman.Repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.text.MessageFormat;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {
    @Autowired
    private ConfirmationTokenService confirmationTokenService;

    @Autowired
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final Optional<User> optionalUser = userRepository.findByEmail(email);
        System.out.println("loading user with email " + email);
        System.out.println("user ispresent " + optionalUser.isPresent());
        return optionalUser.orElseThrow(() ->
                new UsernameNotFoundException(MessageFormat.format("User with email {0} cannot be found.", email)));
    }

    public int signUpUser(User user) {
        if (user.getPassword().length() < 6) {
            return 2;
        }

        final String encryptedPassword = bCryptPasswordEncoder.encode(user.getPassword());

        user.setPassword(encryptedPassword);

        Optional<User> existingUser = userRepository.findByEmail(user.getEmail());
        if (!existingUser.isPresent()) {
            final User createdUser = userRepository.save(user);

            final ConfirmationToken confirmationToken = new ConfirmationToken(user);

            confirmationTokenService.saveConfirmationToken(confirmationToken);

            sendConfirmationMail(createdUser.getEmail(), confirmationToken.getConfirmationToken());
            return 0;
        }
        return 1;

    }

    public void sendConfirmationMail(String userMail, String token) {

        final SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(userMail);
        mailMessage.setSubject("Mail Confirmation Link!");
        mailMessage.setFrom("noreply@miniemag.ro");
        mailMessage.setText(
                "Thank you for registering. Please click on the below link to activate your account." + "http://localhost:8080/sign-up/confirm?token="
                        + token);

        emailSenderService.sendEmail(mailMessage);
    }

    public void confirmUser(ConfirmationToken confirmationToken) {

        final User user = confirmationToken.getUser();

        user.setEnabled(true);

        userRepository.save(user);

        confirmationTokenService.deleteConfirmationToken(confirmationToken.getId());

    }

    public User returnCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            Long id = ((User) principal).getId();
            Optional<User> user = userRepository.findById(id);
            return user.isPresent() ? user.get() : null;
        }
        return null;
    }

//    @Transactional
//    @ModelAttribute("currentUser")
//    public Object getCurrentUser() {
//        System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
//        try {
//            Object u = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            if (u instanceof UserDetails) {
//                return u;
//            }
//            return u;
//        }
//        catch (LazyInitializationException exception) {
//            return null;
//        }
//    }

}

