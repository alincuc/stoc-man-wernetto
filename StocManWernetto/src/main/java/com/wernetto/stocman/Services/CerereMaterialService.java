package com.wernetto.stocman.Services;

import com.wernetto.stocman.Models.CerereMaterial;
import com.wernetto.stocman.Models.CerereMaterialItem;
import com.wernetto.stocman.Models.MaterialStoc;
import com.wernetto.stocman.Repositories.CerereMaterialRepository;
import com.wernetto.stocman.Repositories.MaterialStocRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CerereMaterialService {

    private final MaterialStocRepository materialStocRepository;
    private final CerereMaterialRepository cerereMaterialRepository;

    public CerereMaterialService(MaterialStocRepository materialStocRepository, CerereMaterialRepository cerereMaterialRepository) {
        this.materialStocRepository = materialStocRepository;
        this.cerereMaterialRepository = cerereMaterialRepository;
    }

    public void confirmCerere(CerereMaterial cerereMaterial) {

        for (CerereMaterialItem cerereMaterialItem : cerereMaterial.getCerereMaterialItemSet()) {

            MaterialStoc stocMaterialIn = materialStocRepository.findMaterialStocBySantierAndTipMaterial(cerereMaterial.getSantierIn(), cerereMaterialItem.getTipMaterial());
            stocMaterialIn.setCantitate(stocMaterialIn.getCantitate() + cerereMaterialItem.getCantitate());

            MaterialStoc stocMaterialOut = materialStocRepository.findMaterialStocBySantierAndTipMaterial(cerereMaterial.getSantierOut(), cerereMaterialItem.getTipMaterial());
            stocMaterialOut.setCantitate(stocMaterialOut.getCantitate() - cerereMaterialItem.getCantitate());
            materialStocRepository.save(stocMaterialIn);
            materialStocRepository.save(stocMaterialOut);
            cerereMaterialRepository.save(cerereMaterial);
        }
        cerereMaterial.setStatus("confirmed");

    }

    public void rejectCerere(CerereMaterial cerereMaterial) {
        cerereMaterial.setStatus("reject");
        cerereMaterialRepository.save(cerereMaterial);
    }
}
