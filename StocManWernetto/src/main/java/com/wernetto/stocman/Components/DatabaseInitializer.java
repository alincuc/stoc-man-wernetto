package com.wernetto.stocman.Components;

import com.wernetto.stocman.Models.*;
import com.wernetto.stocman.Repositories.*;
import com.wernetto.stocman.Utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class DatabaseInitializer implements ApplicationRunner {
    private static final Logger LOG =
            LoggerFactory.getLogger(DatabaseInitializer.class);


    private final UserRepository userRepository;

    private final SantierRepository santierRepository;

    private final TipMaterialRepository tipMaterialRepository;

    private final CategorieRepository categorieRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final MaterialStocRepository materialStocRepository;

    public DatabaseInitializer(SantierRepository santierRepository, UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, TipMaterialRepository tipMaterialRepository, CategorieRepository categorieRepository, MaterialStocRepository materialStocRepository) {
        this.santierRepository = santierRepository;
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.tipMaterialRepository = tipMaterialRepository;
        this.categorieRepository = categorieRepository;
        this.materialStocRepository = materialStocRepository;
    }

    @Override
    public void run(ApplicationArguments args) {

        boolean databaseEmpty = santierRepository.findAll().size() == 0 && userRepository.findAll().size() == 0;
        if (databaseEmpty) {

            User admin = new User();
            admin.setEnabled(true);
            admin.setUserRole(UserRole.ADMIN);
            admin.setPrenume("Administrator");
            admin.setNume("Wernetto");
            admin.setEmail("contact@wernetto.ro");
            admin.setPassword(bCryptPasswordEncoder.encode("parolasimpla"));
            admin = userRepository.save(admin);

            Santier santier = new Santier();
            santier.setUserSantier(admin);
            santier.setNume("Magazie");
            santier.setLocatie("0, 0");
            santierRepository.save(santier);

            if (Constants.DEBUG) {

                LOG.info("Application started with option names : {}",
                        args.getOptionNames());
                LOG.info("Initializing database...");

                LOG.info("Database empty, initializing...");

                User tavi = new User();
                tavi.setEnabled(true);
                tavi.setUserRole(UserRole.ADMIN);
                tavi.setEmail("tavi.popoviciu@yahoo.com");
                tavi.setPrenume("Tavi");
                tavi.setNume("Popoiciu");
                tavi.setPassword(bCryptPasswordEncoder.encode("parolasimpla"));
                tavi = userRepository.save(admin);
                userRepository.save(tavi);

                User alin = new User();
                alin.setEnabled(true);
                alin.setUserRole(UserRole.ADMIN);
                alin.setNume("Alin");
                alin.setNume("Cuc");
                alin.setEmail("alin.cucc@gmail.com");
                alin.setPrenume("Administrator");
                alin.setPassword(bCryptPasswordEncoder.encode("Parola.12345"));
                alin = userRepository.save(admin);
                userRepository.save(alin);

                User andrei = new User();
                andrei.setEnabled(true);
                andrei.setUserRole(UserRole.ADMIN);
                andrei.setPrenume("Andrei");
                andrei.setNume("Ixari");
                andrei.setEmail("andreiixari@gmail.com");
                andrei.setPrenume("Administrator");
                andrei.setPassword(bCryptPasswordEncoder.encode("prometeu1992@"));
                andrei = userRepository.save(admin);
                userRepository.save(andrei);
//
//                Santier hotelSport = new Santier(2L, "Hotel Sport", "46.76752550469095, 23.573703369374698", admin, null, null,null);
//                hotelSport = santierRepository.save(hotelSport);
//
//                santierRepository.save(new Santier(3L, "Theodor Mihali", "46.77523826937414, 23.621397071226838", admin, null, null,null));
//                categorieRepository.save(new Categorie(1L, "Popi Metalici", null, null, null));
//                categorieRepository.save(new Categorie(2L, "Popi din Lemn", null, null, null));
//                categorieRepository.save(new Categorie(3L, "Suruburi", null, null, null));
//                categorieRepository.save(new Categorie(4L, "Cuie", null, null, null));

//                Categorie sarma = new Categorie(5L, "Sarma", null, null, null);
//                sarma = categorieRepository.save(sarma);
//
//                TipMaterial sarma14 = new TipMaterial(1L, "Sarma 1.8mm", sarma, null, null);
//                TipMaterial sarma16 = new TipMaterial(2L, "Sarma 1.6mm", sarma, null, null);
//                TipMaterial sarma18 = new TipMaterial(3L, "Sarma 1.8mm", sarma, null, null);
//                TipMaterial sarma20 = new TipMaterial(4L, "Sarma 2.0mm", sarma, null, null);

//                sarma14 = tipMaterialRepository.save(sarma14);
//                sarma16 = tipMaterialRepository.save(sarma16);
//                sarma18 = tipMaterialRepository.save(sarma18);
//                sarma20 = tipMaterialRepository.save(sarma20);

//                materialStocRepository.save(new MaterialStoc(1L, 12, hotelSport, sarma14));
//                materialStocRepository.save(new MaterialStoc(2L, 14, hotelSport, sarma16));
//                materialStocRepository.save(new MaterialStoc(3L, 16, hotelSport, sarma18));
//                materialStocRepository.save(new MaterialStoc(4L, 18, hotelSport, sarma20));

            }

        }
    }
}