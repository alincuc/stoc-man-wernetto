package com.wernetto.stocman.Models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipMaterial {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nume;

    @Column
    private String unitateMasura;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="categorie_id")
    private Categorie categorieTipMaterial;

    @OneToMany(mappedBy = "tipMaterial")
    private Set <MaterialStoc> materialStocSet;

    @OneToMany(mappedBy = "tipMaterial")
    private Set<CerereMaterialItem> cerereItemSet;

}
