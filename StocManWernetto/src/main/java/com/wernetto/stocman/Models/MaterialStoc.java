package com.wernetto.stocman.Models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MaterialStoc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Integer cantitate;

    @ManyToOne
    @JoinColumn(name = "santier_id")
    private Santier santier;

    @ManyToOne
    @JoinColumn(name = "tipMaterial_id")
    private TipMaterial tipMaterial;

}
