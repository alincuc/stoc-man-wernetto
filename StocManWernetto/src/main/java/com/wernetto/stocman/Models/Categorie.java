package com.wernetto.stocman.Models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Categorie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nume;

    @OneToMany(mappedBy = "categorieTipMaterial")
    private Set<TipMaterial>tipMaterialSet=new HashSet<>();

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "parent_category_id")
    private Categorie parentCategory;

    @OneToMany(mappedBy = "parentCategory")
    private Set<Categorie> subcategories = new HashSet<Categorie>();
}
