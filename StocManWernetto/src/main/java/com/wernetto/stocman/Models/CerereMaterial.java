package com.wernetto.stocman.Models;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CerereMaterial {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Date date = new Date();
    @Column
    private String status;
    @ManyToOne
    @JoinColumn(name = "santier_in_id")
    private Santier santierIn;

    @ManyToOne
    @JoinColumn(name = "santier_out_id")
    private Santier santierOut;

    @ManyToOne
    @JoinColumn(name = "user_id")

    private User user;
    @OneToMany(mappedBy = "cerereMaterial")
    private Set<CerereMaterialItem> cerereMaterialItemSet = new HashSet<>();
}