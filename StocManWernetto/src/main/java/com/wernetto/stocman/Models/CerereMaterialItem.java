package com.wernetto.stocman.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CerereMaterialItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Integer cantitate;

    @ManyToOne
    @JoinColumn(name = "tip_material_id")
    private TipMaterial tipMaterial;

    @ManyToOne
    @JoinColumn(name = "cerereMaterial_id")
    private CerereMaterial cerereMaterial;
}