package com.wernetto.stocman.Models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Santier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nume;

    @Column
    private String locatie;

    @Column
    private String photoUrl;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userSantier;

    @OneToMany(mappedBy = "santier")
    private Set<MaterialStoc> materialStocSet = new HashSet<>();

    @OneToMany(mappedBy = "santierIn")
    private Set<CerereMaterial> cerereMaterialIn = new HashSet<>();

    @OneToMany(mappedBy = "santierOut")
    private Set<CerereMaterial> cerereMaterialOut = new HashSet<>();

    @OneToMany(mappedBy = "santier")
    private Set<SantierPhoto> santierPhotoSet = new HashSet<>();

}

