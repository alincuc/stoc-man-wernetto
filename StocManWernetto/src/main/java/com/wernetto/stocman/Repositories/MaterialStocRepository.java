package com.wernetto.stocman.Repositories;

import com.wernetto.stocman.Models.MaterialStoc;
import com.wernetto.stocman.Models.Santier;
import com.wernetto.stocman.Models.TipMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MaterialStocRepository extends JpaRepository<MaterialStoc, Long> {

    MaterialStoc findMaterialStocBySantierAndTipMaterial(Santier santier, TipMaterial tipMaterial);
    List<MaterialStoc> findMaterialStocsBySantier(Santier santier);
}
