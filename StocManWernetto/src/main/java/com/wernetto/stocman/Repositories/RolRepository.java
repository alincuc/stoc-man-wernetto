package com.wernetto.stocman.Repositories;

import com.wernetto.stocman.Models.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolRepository extends JpaRepository<Rol, Long> {
}
