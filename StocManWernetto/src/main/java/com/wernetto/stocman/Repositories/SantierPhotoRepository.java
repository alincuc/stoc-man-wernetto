package com.wernetto.stocman.Repositories;

import com.wernetto.stocman.Models.Santier;
import com.wernetto.stocman.Models.SantierPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SantierPhotoRepository extends JpaRepository<SantierPhoto, Long> {

    List<SantierPhoto> findSantierPhotoBySantier(Santier santier);

}
