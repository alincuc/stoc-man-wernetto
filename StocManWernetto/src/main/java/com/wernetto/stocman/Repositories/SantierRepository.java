package com.wernetto.stocman.Repositories;


import com.wernetto.stocman.Models.Santier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SantierRepository extends JpaRepository<Santier, Long>  {
}
