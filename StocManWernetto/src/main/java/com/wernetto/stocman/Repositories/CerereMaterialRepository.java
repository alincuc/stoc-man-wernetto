package com.wernetto.stocman.Repositories;

import com.wernetto.stocman.Models.CerereMaterial;
import com.wernetto.stocman.Models.CerereMaterialItem;
import com.wernetto.stocman.Models.Santier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CerereMaterialRepository extends JpaRepository<CerereMaterial, Long> {

    List <CerereMaterial> findAllBySantierInIs(Santier santier);
    List <CerereMaterial> findAllBySantierOutIs(Santier santier);
}
