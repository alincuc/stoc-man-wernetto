package com.wernetto.stocman.Repositories;

import com.wernetto.stocman.Models.TipMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipMaterialRepository extends JpaRepository<TipMaterial, Long> {
}
