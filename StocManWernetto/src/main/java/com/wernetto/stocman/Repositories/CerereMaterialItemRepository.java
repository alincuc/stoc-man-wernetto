package com.wernetto.stocman.Repositories;

import com.wernetto.stocman.Models.CerereMaterialItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CerereMaterialItemRepository extends JpaRepository<CerereMaterialItem, Long> {
}