package com.wernetto.stocman.Controllers;


import com.wernetto.stocman.Models.MaterialStoc;
import com.wernetto.stocman.Repositories.MaterialStocRepository;
import com.wernetto.stocman.Repositories.SantierRepository;
import com.wernetto.stocman.Repositories.TipMaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/materialStoc")
public class MaterialStocController {


    private final MaterialStocRepository materialStocRepositoryl;

    private final SantierRepository santierRepository;

    private final TipMaterialRepository tipMaterialRepository;

    public MaterialStocController(MaterialStocRepository materialStocRepositoryl, SantierRepository santierRepository, TipMaterialRepository tipMaterialRepository) {
        this.materialStocRepositoryl = materialStocRepositoryl;
        this.santierRepository = santierRepository;
        this.tipMaterialRepository = tipMaterialRepository;
    }

    @PostMapping("/handleAdd")
    private String handleAddMaterialStoc(MaterialStoc materialStoc) {
        materialStocRepositoryl.save(materialStoc);
        return "redirect:/santier/magazie?santier_id="+materialStoc.getSantier().getId();
    }

    @GetMapping("/add") //create
    private String showAddMaterialStoc(Model model) {
        model.addAttribute("materialStoc", new MaterialStoc());
        model.addAttribute("santiere", santierRepository.findAll());
        model.addAttribute("tipMateriale", tipMaterialRepository.findAll());
        return "material/add_materialStoc";
    }

    @GetMapping("/") //read
    private String MaterialStocList(Model model) {
        List<MaterialStoc> materialStocList = materialStocRepositoryl.findAll();
        model.addAttribute("materialStoc", materialStocList);
        return "material/materialStoc";
    }

    @GetMapping("/update") //update
    private String editMaterialStoc(@RequestParam("materialStoc_id") Long materialStocId, Model model) {
        Optional<MaterialStoc> materialStocOptional = materialStocRepositoryl.findById(materialStocId);
        MaterialStoc materialStoc = materialStocOptional.isPresent() ? materialStocOptional.get() : new MaterialStoc();
        model.addAttribute("materialStoc", materialStoc);
        model.addAttribute("santiere", santierRepository.findAll());
        model.addAttribute("tipMateriale",tipMaterialRepository.findAll());
        return "material/add_materialStoc";
    }

    @PostMapping("/handleUpdate")
    private String handleUpdateMaterialStoc(MaterialStoc materialStoc) {
        materialStocRepositoryl.save(materialStoc);
        return "redirect:/santier/list";

    }


    @PostMapping("/delete") //delete
    private String delete(@RequestParam("materialStoc_id") Long materialStocId, Model model) {
        materialStocRepositoryl.deleteById(materialStocId);
        return "redirect:/santier/list";
    }


}
