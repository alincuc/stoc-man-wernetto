package com.wernetto.stocman.Controllers;

import com.wernetto.stocman.Models.CerereMaterial;
import com.wernetto.stocman.Models.CerereMaterialItem;
import com.wernetto.stocman.Models.Santier;
import com.wernetto.stocman.Models.TipMaterial;
import com.wernetto.stocman.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/cerereMaterial")
public class CerereMaterialController {
    @Autowired
    private CerereMaterialRepository cerereMaterialRepository;
    @Autowired
    private SantierRepository santierRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TipMaterialRepository tipMaterialRepository;
    @Autowired
    private CerereMaterialItemRepository cerereMaterialItemRepository;

    @PostMapping("/handleAdd")
    private String handleAddCerereMaterial(CerereMaterial cerereMaterial) {
        cerereMaterialRepository.save(cerereMaterial);
        return "redirect:/cerereMaterial/list";
    }

    @GetMapping("/add") //create
    private String showAddCerereMaterialPage(Model model) {
        model.addAttribute("cerereMaterial", new CerereMaterial());
        model.addAttribute("useri", userRepository.findAll());
        model.addAttribute("santiere", santierRepository.findAll());
        return "cerereMaterial/add_cerereMaterial";
    }


    @GetMapping("/update") //update
    private String editCerereMaterial(@RequestParam("cerereMaterial_id") Long cerereMaterialId, Model model) {
        Optional<CerereMaterial> cerereMaterialOptional = cerereMaterialRepository.findById(cerereMaterialId);
        CerereMaterial cerereMaterial = cerereMaterialOptional.isPresent() ? cerereMaterialOptional.get() : new CerereMaterial();
        model.addAttribute("cerereMaterial", cerereMaterial);
        model.addAttribute("useri", userRepository.findAll());
        model.addAttribute("santiere", santierRepository.findAll());
        model.addAttribute("cerereMaterialItem", cerereMaterialItemRepository.findAll());
        return "cerereMaterial/add_cerereMaterial";
    }

    @PostMapping("/handleUpdate")
    private String handleUpdateCerereMaterial(CerereMaterial cerereMaterial) {
        cerereMaterialRepository.save(cerereMaterial);
        return "redirect:/cerereMaterial/list";
    }

    @PostMapping("/delete") //delete
    private String delete(@RequestParam("cerereMaterial_id") Long cerereMaterialId, Model model) {
        cerereMaterialRepository.deleteById(cerereMaterialId);
        return "redirect:/cerereMaterial/list";
    }

    @PostMapping("/cerere_material_and_items")
    private String cerereMaterialAndItems(@RequestParam("santierIn") Long santierInId, @RequestParam("santierOut") Long santierOutId,
                                          @RequestParam("tipMaterial") List<Long> tipMaterialIdList, @RequestParam("cantitate") List<Long> cantitateList) {
        CerereMaterial cerereMaterial = new CerereMaterial();
        cerereMaterial.setSantierIn(santierRepository.findById(santierInId).orElse(new Santier()));
        cerereMaterial.setSantierOut(santierRepository.findById(santierOutId).orElse(new Santier()));
        cerereMaterial.setStatus("Open");
        cerereMaterialRepository.save(cerereMaterial);
        for (int i = 0; i < tipMaterialIdList.size(); i++) {
            Long tipMaterialId = tipMaterialIdList.get(i);
            Long cantitate = cantitateList.get(i);
            CerereMaterialItem cerereMaterialItem = new CerereMaterialItem();
            cerereMaterialItem.setTipMaterial(tipMaterialRepository.findById(tipMaterialId).orElse(new TipMaterial()));
            cerereMaterialItem.setCantitate(Math.toIntExact(cantitate));
            cerereMaterialItem.setCerereMaterial(cerereMaterial);
            cerereMaterialItemRepository.save(cerereMaterialItem);
        }
        return "redirect:/";
    }

    @GetMapping("/cerere_material_and_items")
    private String cerereMaterialAndItemsPage(Model model) {
        model.addAttribute("santiere", santierRepository.findAll());
        model.addAttribute("tipMateriale", tipMaterialRepository.findAll());

        return "cerereMaterial/cerere_material_and_items";
    }
}

