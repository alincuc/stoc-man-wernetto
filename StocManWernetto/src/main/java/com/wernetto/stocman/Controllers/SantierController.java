package com.wernetto.stocman.Controllers;

import com.wernetto.stocman.Models.CerereMaterial;
import com.wernetto.stocman.Models.CerereMaterialItem;
import com.wernetto.stocman.Models.Santier;
import com.wernetto.stocman.Models.SantierPhoto;
import com.wernetto.stocman.Repositories.*;
import com.wernetto.stocman.Services.CerereMaterialService;
import com.wernetto.stocman.Services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/santier")
public class SantierController {

    private final SantierRepository santierRepository;
    private final UserRepository userRepository;
    private final MaterialStocRepository materialStocRepository;
    private final CerereMaterialItemRepository cerereMaterialItemRepository;
    private final CerereMaterialRepository cerereMaterialRepository;
    private final TipMaterialRepository tipMaterialRepository;
    private final CerereMaterialService cerereMaterialService;
    private final UserService userService;
    private final SantierPhotoRepository santierPhotoRepository;

    public SantierController(SantierRepository santierRepository, UserRepository userRepository, MaterialStocRepository materialStocRepository, CerereMaterialItemRepository cerereMaterialItemRepository, CerereMaterialRepository cerereMaterialRepository, TipMaterialRepository tipMaterialRepository, UserService userService, CerereMaterialService cerereMaterialService, SantierPhotoRepository santierPhotoRepository) {
        this.santierRepository = santierRepository;
        this.userRepository = userRepository;
        this.materialStocRepository = materialStocRepository;
        this.tipMaterialRepository = tipMaterialRepository;
        this.cerereMaterialItemRepository = cerereMaterialItemRepository;
        this.cerereMaterialRepository = cerereMaterialRepository;
        this.userService = userService;
        this.cerereMaterialService = cerereMaterialService;
        this.santierPhotoRepository = santierPhotoRepository;
    }

    @GetMapping("/")
    private String homePage() {
        return "redirect:/santier/list";
    }

    @PostMapping("/handleAdd")
    private String handleAddSantier(Santier santier) {
        santierRepository.save(santier);
        return "redirect:/santier/list";
    }

    @GetMapping("/list") //read
    private String santierList(Model model) {
        List<Santier> santierList = santierRepository.findAll();
        model.addAttribute("santiere", santierList);
        return "santier/santiere";
    }

    @GetMapping("/add") //create
    private String showAddSantierPage(Model model) {
        model.addAttribute("santier", new Santier());
        model.addAttribute("useri", userRepository.findAll());
        return "santier/add_santier";
    }

    @GetMapping("/update") //update
    private String editSantier(@RequestParam("santier_id") Long santierId, Model model) {
        Optional<Santier> santierOptional = santierRepository.findById(santierId);
        Santier santier = santierOptional.orElseGet(Santier::new);
        model.addAttribute("useri", userRepository.findAll());
        model.addAttribute("santier", santier);
        return "santier/add_santier";
    }

    @PostMapping("/delete") //delete
    private String delete(@RequestParam("santier_id") Long santierId) {
        santierRepository.deleteById(santierId);
        return "redirect:/santier/list";
    }

    @GetMapping("/detaliu")
    private String detaliuSantier(@RequestParam("santier_id") Long santierId, Model model) {
        Optional<Santier> santierOptional = santierRepository.findById(santierId);
        Santier santier = santierOptional.orElseGet(Santier::new);
        model.addAttribute("santier", santier);
        model.addAttribute("currentUser", userService.returnCurrentUser());
        return "santier/detaliuSantier";
    }

    @GetMapping("/magazie")
    private String magazie(@RequestParam("santier_id") Long santierId, Model model) {
        Santier santier = santierRepository.findById(santierId).orElse(new Santier());
        System.out.println(santier.getUserSantier());
        System.out.println(userService.returnCurrentUser());
        model.addAttribute("santier", santier);
        model.addAttribute("materialStocuri", materialStocRepository.findMaterialStocsBySantier(santier));
        model.addAttribute("currentUser", userService.returnCurrentUser());
        return "santier/magazie";
    }

    @PostMapping("/confirmCerere")
    private String confirm(@RequestParam("id") Long cerereMaterialId) {
        CerereMaterial cerereMaterial = cerereMaterialRepository.findById(cerereMaterialId).orElse(new CerereMaterial());
        cerereMaterialService.confirmCerere(cerereMaterial);
        return "redirect:/santier/magazie?santier_id=" + cerereMaterial.getSantierOut().getId();
    }

    @PostMapping("/rejectCerere")
    private String reject(@RequestParam("id") Long cerereMaterialId) {
        CerereMaterial cerereMaterial = cerereMaterialRepository.findById(cerereMaterialId).orElse(new CerereMaterial());
        cerereMaterialService.rejectCerere(cerereMaterial);
        return "redirect:/santier/magazie?santier_id=" + cerereMaterial.getSantierOut().getId();
    }

    @GetMapping("/cerere_and_items_santier")
    private String cerereAndItem(@RequestParam("santier_id") Long santierId, Model model) {
        Santier santier = santierRepository.findById(santierId).orElse(new Santier());
        model.addAttribute("santier", santier);
        List<Santier> santierList = santierRepository.findAll();
        santierList.remove(santier);
        model.addAttribute("santiere", santierList);
        model.addAttribute("tipMateriale", tipMaterialRepository.findAll());
        model.addAttribute("currentUser", userService.returnCurrentUser());
        return "santier/cerere_and_items_santier";
    }

    @GetMapping("/cereriMaterial") //read
    private String cerereMaterialList(@RequestParam("santier_id") Long santierId, Model model) {
        List<CerereMaterialItem> cerereMaterialItemList = cerereMaterialItemRepository.findAll();
        model.addAttribute("cereriMaterial", cerereMaterialItemList);
        model.addAttribute("santiere", santierRepository.findAll());
        Santier santier = santierRepository.findById(santierId).orElse(new Santier());
        model.addAttribute("santier", santier);
        List<CerereMaterial> cerereMaterialeTrimise = cerereMaterialRepository.findAllBySantierInIs(santier);
        List<CerereMaterial> cerereMaterialePrimite = cerereMaterialRepository.findAllBySantierOutIs(santier);
        model.addAttribute("cerereMaterialeTrimise", cerereMaterialeTrimise);
        model.addAttribute("cerereMaterialePrimite", cerereMaterialePrimite);
        model.addAttribute("currentUser", userService.returnCurrentUser());
        return "cerereMaterial/cereriMaterialItem";
    }

    @GetMapping("/photoList")
    private String photoList(@RequestParam("santier_id") Long santierId, Model model) {
        Santier santier = santierRepository.findById(santierId).orElse(new Santier());
        List<SantierPhoto> santierPhotoList = santierPhotoRepository.findSantierPhotoBySantier(santier);
        model.addAttribute("photosSantier", santierPhotoList);
        model.addAttribute("santier", santier);
        return "santier/foto_santier";
    }

    @GetMapping("/addPhoto")
    private String adaugaPhoto( @RequestParam("santier_id") Long santierId, Model model) {
        model.addAttribute("photoSantier", new SantierPhoto());
        Santier santier = santierRepository.findById(santierId).orElse(new Santier());
        model.addAttribute("santier", santier);
        return "santier/add_photoSantier";
    }

    @PostMapping("/handleAddPhotoSantiere")
    private String handelAddPhotoSantiere(SantierPhoto santierPhoto, Santier santier) {
        santierPhotoRepository.save(santierPhoto);
        santierRepository.save(santier);
        return "redirect:/santier/foto_santier";
    }

}
