package com.wernetto.stocman.Controllers;


import com.wernetto.stocman.Models.Categorie;
import com.wernetto.stocman.Repositories.CategorieRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/categorie")
public class CategorieController {

    private final CategorieRepository categorieRepository;

    public CategorieController(CategorieRepository categorieRepository) {
        this.categorieRepository = categorieRepository;
    }

    @GetMapping("/") //read
    private String home() {
        return "redirect:/categorie/list";
    }

    @GetMapping("/list") //read all
    private String categorieList(Model model) {
        List<Categorie> categorieList = categorieRepository.findAll();
        model.addAttribute("categorii", categorieList);
        return "santier/categorii";
    }

    @GetMapping("/add") //load create page
    private String showAddCategoriePage(Model model) {
        model.addAttribute("categorie", new Categorie());
        return "santier/add_categorie";
    }

    @PostMapping("/handleAdd") //handle post request from create page
    private String handleAddCategorie(Categorie categorie) {
        categorieRepository.save(categorie);
        return "redirect:/categorie/list";
    }

    @GetMapping("/update") //load update page
    private String editCategorie(@RequestParam("categorie_id") Long categorieId, Model model) {
        Optional<Categorie> categorieOptional = categorieRepository.findById(categorieId);
        Categorie categorie = categorieOptional.orElseGet(Categorie::new);
        model.addAttribute("categorie", categorie);
        return "santier/add_categorie";
    }

    @PostMapping("/handleUpdate") //hanle post request from update page
    private String handleUpdateCategorie(Categorie categorie) {
        categorieRepository.save(categorie);
        return "redirect:/categorie/list";
    }


    @PostMapping("/delete") //handle post request from list page
    private String delete(@RequestParam("categorie_id") Long categorieId) {
        categorieRepository.deleteById(categorieId);
        return "redirect:/categorie/list";
    }

}
