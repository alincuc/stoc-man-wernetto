package com.wernetto.stocman.Controllers;


import com.wernetto.stocman.Models.CerereMaterialItem;
import com.wernetto.stocman.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;


@Controller
@RequestMapping("/cerereMaterialItem")
public class CerereMaterialItemController {

    @Autowired
    CerereMaterialItemRepository cerereMaterialItemRepository;

    @Autowired
    TipMaterialRepository tipMaterialRepository;

    @Autowired
    SantierRepository santierRepository;

    @Autowired
    UserRepository userRepository;

    @PostMapping("/handleAdd")
    private String handleAddCerereMaterial(CerereMaterialItem cerereMaterialItem) {
        cerereMaterialItemRepository.save(cerereMaterialItem);
        return "redirect:/cerereMaterialItem/list";
    }


//    @GetMapping("/add") //create
//    private String showAddCerereMaterialPage(Model model) {
//        model.addAttribute("cerereMaterialItem", new CerereMaterialItem());
//        model.addAttribute("tipMaterial", tipMaterialRepository.findAll());
//        return "cerereMaterial/add_cerereMaterialItem";
//    }

//    @GetMapping("/list") //read
//    private String CerereMaterialList(Model model) {
//        List<CerereMaterialItem> cerereMaterialItemList = cerereMaterialItemRepository.findAll();
//        model.addAttribute("cereriMaterial", cerereMaterialItemList);
//        return "cerereMaterial/cereriMaterialItem";
//    }


    @GetMapping("/update") //update
    private String editCerereMaterial(@RequestParam("cerereMaterialItem_id") Long cerereMaterialItemId, Model model) {
        Optional<CerereMaterialItem> cerereMaterialItemOptional = cerereMaterialItemRepository.findById(cerereMaterialItemId);
        CerereMaterialItem cerereMaterialItem = cerereMaterialItemOptional.isPresent() ? cerereMaterialItemOptional.get() : new CerereMaterialItem();
        model.addAttribute("cerereMaterialItem", cerereMaterialItem);
        model.addAttribute("tipMaterial", tipMaterialRepository.findAll());
        return "cererematerial/add_cerereMaterialItem";
    }

    @PostMapping("/handleUpdate")
    private String handleUpdateCerereMaterial(CerereMaterialItem cerereMaterialItem) {
        cerereMaterialItemRepository.save(cerereMaterialItem);
        return "redirect:/cerereMaterialItem/list";

    }


    @PostMapping("/delete") //delete
    private String delete(@RequestParam("cerereMaterialItem_id") Long cerereMaterialItemId, Model model) {
        cerereMaterialItemRepository.deleteById(cerereMaterialItemId);
        return "redirect:/cerereMaterialItem/list";
    }
}