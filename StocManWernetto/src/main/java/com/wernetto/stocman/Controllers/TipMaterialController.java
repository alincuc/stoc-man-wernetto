package com.wernetto.stocman.Controllers;


import com.wernetto.stocman.Models.MaterialStoc;
import com.wernetto.stocman.Models.TipMaterial;
import com.wernetto.stocman.Repositories.CategorieRepository;
import com.wernetto.stocman.Repositories.MaterialStocRepository;
import com.wernetto.stocman.Repositories.TipMaterialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/tipMaterial")
public class TipMaterialController {

    @Autowired
    private TipMaterialRepository tipMaterialRepository;

    @Autowired
    private CategorieRepository categorieRepository;

    @Autowired
    private MaterialStocRepository materialStocRepository;

    @PostMapping("/handleAdd")
    private String handleAddTipMaterial(TipMaterial tipMaterial, MaterialStoc materialStoc) {
        tipMaterialRepository.save(tipMaterial);
        materialStocRepository.save(materialStoc);
        return "redirect:/tipMaterial/list";
    }

    @GetMapping("/add") //create
    private String showAddTipMaterialPage(Model model) {
        model.addAttribute("tipMaterial", new TipMaterial());
        model.addAttribute("categorii", categorieRepository.findAll());
        return "material/add_tipMaterial";
    }

    @GetMapping("/list") //read
    private String tipMaterialList(Model model) {
        List<TipMaterial> tipMaterialList = tipMaterialRepository.findAll();
        model.addAttribute("tipMateriale", tipMaterialList);
        return "material/tipMaterial";
    }

    @GetMapping("/update") //update
    private String editTipMaterial(@RequestParam("tipMaterial_id") Long tipMaterialId, Model model) {
        Optional<TipMaterial> tipMaterialOptional = tipMaterialRepository.findById(tipMaterialId);
        TipMaterial tipMaterial = tipMaterialOptional.isPresent() ? tipMaterialOptional.get() : new TipMaterial();
        model.addAttribute("tipMaterial", tipMaterial);
        model.addAttribute("categorii", categorieRepository.findAll());
        return "material/add_tipMaterial";
    }

    @PostMapping("/handleUpdate")
    private String handleUpdateTipMaterial(TipMaterial tipMaterial) {
        tipMaterialRepository.save(tipMaterial);
        return "redirect:/tipMaterial/list";
    }

    @PostMapping("/delete") //delete
    private String delete(@RequestParam("tipMaterial_id") Long tipMaterialId, Model model) {
        tipMaterialRepository.deleteById(tipMaterialId);
        return "redirect:/tipMaterial/list";
    }


}
